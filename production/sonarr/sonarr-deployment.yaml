apiVersion: apps/v1
kind: Deployment
metadata:
  name: sonarr
  labels:
    app: sonarr
    tier: frontend
spec:
  selector:
    matchLabels:
      app: sonarr
      tier: frontend
  replicas: 1
  template:
    metadata:
      labels:
        app: sonarr
        tier: frontend
    spec:
      containers:
      - name: sonarr-s3fs
        securityContext:
          privileged: true
        image: panubo/s3fs:1.84
        imagePullPolicy: Always
        volumeMounts:
        - name: sonarr-s3fs
          mountPath: /data
          mountPropagation: Bidirectional
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: AWS_S3_MOUNTPOINT
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_mount_point
        - name: S3FS_ARGS
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_s3fs_args
        - name: AWS_S3_URL
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_s3_url
        - name: AWS_STORAGE_BUCKET_NAME
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_bucket_name
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_access_key_id
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: sonarr
              key: sonarr-s3fs.aws_secret_access_key
      - name: sonarr
        image: linuxserver/sonarr:3.0.3.655-ls150
        imagePullPolicy: Always
        volumeMounts:
        - name: sonarr-config
          mountPath: /config
        - name: sonarr-s3fs
          mountPath: /tv
          mountPropagation: HostToContainer
        - name: qbittorrent-complete
          mountPath: /downloads/complete
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: PUID
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr.puid
        - name: PGID
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr.pgid
        - name: UMASK_SET
          valueFrom:
            configMapKeyRef:
              name: sonarr
              key: sonarr.umask
      volumes:
      - name: sonarr-config
        hostPath:
          path: /home/wpuckering/storage/sonarr/config
          type: Directory
      - name: sonarr-s3fs
        emptyDir: {}
      - name: qbittorrent-complete
        hostPath:
          path: /home/wpuckering/storage/qbittorrent/complete
          type: Directory
---
apiVersion: v1
kind: Service
metadata:
  name: sonarr
  labels:
    app: sonarr
    tier: frontend
spec:
  selector:
    app: sonarr
    tier: frontend
  ports:
  - name: http
    port: 8989
    targetPort: 8989
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: sonarr
  labels:
    app: sonarr
    tier: frontend
  annotations:
    prometheus.io/port: "10254"
    prometheus.io/scrape: "true"
    traefik.ingress.kubernetes.io/preserve-host: "true"
    ingress.kubernetes.io/content-type-nosniff: "true"
    ingress.kubernetes.io/custom-browser-xss-value: "1; mode=block"
    ingress.kubernetes.io/custom-frame-options-value: "SAMEORIGIN"
    ingress.kubernetes.io/referrer-policy: "Referrer-Policy: strict-origin-when-cross-origin"
spec:
  tls:
    - hosts:
      - sn.williampuckering.com
      secretName: williampuckering-com
  rules:
    - host: sn.williampuckering.com
      http:
        paths:
        - path: /
          backend:
            serviceName: sonarr
            servicePort: 8989