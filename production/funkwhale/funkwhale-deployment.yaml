apiVersion: apps/v1
kind: Deployment
metadata:
  name: funkwhale
  labels:
    app: funkwhale
    tier: frontend
spec:
  selector:
    matchLabels:
      app: funkwhale
      tier: frontend
  replicas: 1
  template:
    metadata:
      labels:
        app: funkwhale
        tier: frontend
    spec:
      containers:
      - name: funkwhale-s3fs
        securityContext:
          privileged: true
        image: panubo/s3fs:1.84
        imagePullPolicy: Always
        volumeMounts:
        - name: funkwhale-s3fs
          mountPath: /data
          mountPropagation: Bidirectional
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: AWS_S3_MOUNTPOINT
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_mount_point
        - name: S3FS_ARGS
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_s3fs_args
        - name: AWS_S3_URL
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_s3_url
        - name: AWS_STORAGE_BUCKET_NAME
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_bucket_name
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_access_key_id
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: funkwhale
              key: funkwhale-s3fs.aws_secret_access_key
      - name: funkwhale
        image: funkwhale/all-in-one:0.19.1
        imagePullPolicy: Always
        volumeMounts:
        - name: funkwhale-s3fs
          mountPath: /music
          mountPropagation: HostToContainer
        - name: funkwhale-config
          mountPath: /data
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: PUID
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale.puid
        - name: PGID
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale.pgid
        - name: FUNKWHALE_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale.hostname
        - name: NGINX_MAX_BODY_SIZE
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale.nginx_max_body_size
        - name: NESTED_PROXY
          valueFrom:
            configMapKeyRef:
              name: funkwhale
              key: funkwhale.nested_proxy
        - name: DJANGO_SECRET_KEY
          valueFrom:
            secretKeyRef:
              name: funkwhale
              key: funkwhale.django_secret_key
      volumes:
      - name: funkwhale-s3fs
        emptyDir: {}
      - name: funkwhale-config
        hostPath:
          path: /home/wpuckering/storage/funkwhale
          type: Directory
---
apiVersion: v1
kind: Service
metadata:
  name: funkwhale
  labels:
    app: funkwhale
    tier: frontend
spec:
  selector:
    app: funkwhale
    tier: frontend
  ports:
  - name: http
    port: 80
    targetPort: 80
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: funkwhale
  labels:
    app: funkwhale
    tier: frontend
  annotations:
    prometheus.io/port: "10254"
    prometheus.io/scrape: "true"
    traefik.ingress.kubernetes.io/preserve-host: "true"
    ingress.kubernetes.io/content-type-nosniff: "true"
    ingress.kubernetes.io/custom-browser-xss-value: "1; mode=block"
    ingress.kubernetes.io/custom-frame-options-value: "SAMEORIGIN"
    ingress.kubernetes.io/referrer-policy: "Referrer-Policy: strict-origin-when-cross-origin"
spec:
  tls:
    - hosts:
      - fw.williampuckering.com
      secretName: williampuckering-com
  rules:
    - host: fw.williampuckering.com
      http:
        paths:
        - path: /
          backend:
            serviceName: funkwhale
            servicePort: 80