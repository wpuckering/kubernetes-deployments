apiVersion: apps/v1
kind: Deployment
metadata:
  name: teedy
  labels:
    app: teedy
    tier: frontend
spec:
  selector:
    matchLabels:
      app: teedy
      tier: frontend
  replicas: 1
  template:
    metadata:
      labels:
        app: teedy
        tier: frontend
    spec:
      initContainers:
      - name: teedy-init
        image: wpuckering/postgres-backup-s3:latest
        imagePullPolicy: Always
        command: ['sh', '-c', 'export PGPASSWORD=$POSTGRES_PASSWORD; until psql -h $POSTGRES_HOSTNAME -p $POSTGRES_PORT -d $POSTGRES_DB -U $POSTGRES_USER --no-password -c "SELECT 1"; do sleep 5; done; sleep 5;']
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: POSTGRES_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.postgres.hostname
        - name: POSTGRES_PORT
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.postgres.port
        - name: POSTGRES_DB
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.postgres.db
        - name: POSTGRES_USER
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.postgres.user
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.postgres.password
      containers:
      - name: teedy-s3fs
        securityContext:
          privileged: true
        image: panubo/s3fs:1.84
        imagePullPolicy: Always
        volumeMounts:
        - name: teedy-s3fs
          mountPath: /data
          mountPropagation: Bidirectional
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: AWS_S3_MOUNTPOINT
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy-s3fs.aws_mount_point
        - name: S3FS_ARGS
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy-s3fs.aws_s3fs_args
        - name: AWS_S3_URL
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy-s3fs.aws_s3_url
        - name: AWS_STORAGE_BUCKET_NAME
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy-s3fs.aws_bucket_name
        - name: AWS_ACCESS_KEY_ID
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy-s3fs.aws_access_key_id
        - name: AWS_SECRET_ACCESS_KEY
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy-s3fs.aws_secret_access_key
      - name: teedy
        image: sismics/docs:v1.7
        imagePullPolicy: Always
        volumeMounts:
        - name: teedy-s3fs
          mountPath: /data
          mountPropagation: HostToContainer
        env:
        - name: TZ
          valueFrom:
            configMapKeyRef:
              name: common
              key: common.time_zone
        - name: DOCS_SMTP_HOSTNAME
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.smtp.hostname
        - name: DOCS_SMTP_PORT
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.smtp.port
        - name: DOCS_BASE_URL
          valueFrom:
            configMapKeyRef:
              name: teedy
              key: teedy.base_url
        - name: DATABASE_URL
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.database_url
        - name: DATABASE_USER
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.postgres.user
        - name: DATABASE_PASSWORD
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.postgres.password
        - name: DOCS_SMTP_USERNAME
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.smtp.username
        - name: DOCS_SMTP_PASSWORD
          valueFrom:
            secretKeyRef:
              name: teedy
              key: teedy.smtp.password
      volumes:
      - name: teedy-s3fs
        emptyDir: {}
---
apiVersion: v1
kind: Service
metadata:
  name: teedy
  labels:
    app: teedy
    tier: frontend
spec:
  selector:
    app: teedy
    tier: frontend
  ports:
  - name: http
    port: 8080
    targetPort: 8080
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: teedy
  labels:
    app: teedy
    tier: frontend
  annotations:
    prometheus.io/port: "10254"
    prometheus.io/scrape: "true"
    traefik.ingress.kubernetes.io/preserve-host: "true"
    ingress.kubernetes.io/content-type-nosniff: "true"
    ingress.kubernetes.io/custom-browser-xss-value: "1; mode=block"
    ingress.kubernetes.io/custom-frame-options-value: "SAMEORIGIN"
    ingress.kubernetes.io/referrer-policy: "Referrer-Policy: strict-origin-when-cross-origin"
spec:
  tls:
    - hosts:
      - td.williampuckering.com
      secretName: williampuckering-com
  rules:
    - host: td.williampuckering.com
      http:
        paths:
        - path: /
          backend:
            serviceName: teedy
            servicePort: 8080